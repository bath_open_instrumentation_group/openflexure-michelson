/* A monolithic Michelson interferometer base

(c) Richard Bowman 2019, released under CERN OHWL

*/
use <utilities.scad>;

baseline = 80; //nominal length of the arms of the interferometer
linear_travel = 15; //nominal distance the carriage can move

base_t = 25; //thickness of the base
beam_z = 55; //height of the beam above the floor
xflex = [4,1.5,0.75]; //dimensions of a rectangular flexure bending around x axis
zflex = [0.8, 1.5, 4];//dimensions of a rectangular flexure bending around z axis

mirror_pivot = [0, baseline, beam_z]; //the centre of the static (i.e. tilting) mirror
mirror_holder_w = 25+2*5; //width of the block that holds the mirror
mirror_holder_t = 10; //thickness of the mirror holder
mirror_tilt = 3; //maximum tilt of the mirror in degrees

mirror_lever_az_l = 75; //length of the actuating lever 

mirror_bottom_travel = sin(mirror_tilt)*beam_z; //distance travelled by the bottom of the mirror mount
mirror_pushstick_ax_length = 10+2*mirror_bottom_travel-4; //length of the rod connecting mirror to mirror_lever_ax
mirror_actuator_travel = mirror_lever_az_l*sin(mirror_tilt); //distance travelled by the mirror actuators
base_max_y = 130; //+y edge of the base

linear_flexure_z_l = 20; //length of the linear flexures

d = 0.05;

module nut_seat_y(length){
    // A beam along the Y axis in which you can embed a nut, to actuate a lever.
    // length: distance from the -y end of the beam to the centre of the nut
    // the nut is centred on (0,0,base_t/2)
    w = 10;
    difference(){
        hull(){
            cylinder(d=w, h=d, $fn=12);
            translate([-w/2,-w/2,base_t/2-w/2]) cube([w,w,base_t/2+w/2]);
            translate([0, -length+w/2,0]) cylinder(d=w, h=base_t, $fn=12);
        }

        translate([0,0,base_t/2]) nut_y(3, center=true, shaft=true, shaft_length=length-w/2, top_access=true);
    }
}

////////////////////////////// HERE STARTETH THE MIRROR MOUNT //////////////////////////////////////////

mirror_holder_ax_anchor = zeroz(mirror_pivot) + [0, mirror_bottom_travel + xflex[1]/2, 0]; //where the "pushstick" connects
mirror_holder_back = mirror_holder_ax_anchor + [0,mirror_holder_t,0]; //a point on the back of the mirror holder
module mirror_holder(){
    //A block that holds the mirror.  This tilts along 2 axes as per mirror_locus
    w = mirror_holder_w;
    t = mirror_holder_t;
    difference(){
        // base cuboid
        hull(){ // slightly bent shape so we don't foul mirror_lever_az
            translate(mirror_holder_ax_anchor + [-w/2, 0, 0]) cube([w, t, d]);
            translate(mirror_pivot + [-w/2, xflex[1]/2, 0]) cube([w, t, 25/2 + 5]);
        }
        // hole for the mirror
        translate(mirror_pivot) rotate([-90,0,0]) cylinder(d=27.4, h=7, $fn=32);
        translate(mirror_pivot + [0,t/2-1,0]) rotate([0,0,0]) trylinder_selftap(h=999);

        // clearance from the actuating lever - not needed because we've bent the base shape slightly
        //grow() mirror_locus(angle_z=0) mirror_lever_az();
        grow() hull() mirror_locus() mirror_az_anchor();
    }
    translate(mirror_pivot + [0, 7, 8])sphere(r=1,$fn=12);
    translate(mirror_pivot + [6.9, 7, -4])sphere(r=1,$fn=12);
    translate(mirror_pivot + [-6.9, 7, -4])sphere(r=1,$fn=12);
    translate(mirror_pivot + [6.9,1.5,-11.9])rotate([-90,0,0])cylinder(d=2, h=5, $fn=12);
    translate(mirror_pivot + [-6.9,1.5,-11.9])rotate([-90,0,0])cylinder(d=2, h=5, $fn=12);
}

module grow(shape="cylinder", r=1.5, h=4, dz=0){
    // grow a shape by taking the minkowski product with a cylinder (or sphere)
    minkowski(){
        union() children();
        translate([0,0,dz]) if(shape=="cylinder"){
            cylinder(r=r, h=h, $fn=8, center=true);
        }else{
            sphere(r=r, $fn=8);
        }
    }
}

module mirror_locus(angle_x=mirror_tilt, angle_z=mirror_tilt){
    // Rotate the children about the mirror pivot and take a hull
    translate(mirror_pivot) for(ax=[-1,0,1]) for(az=[-1,0,1]){
        rotate([0,0,az*angle_z]) rotate([ax*angle_x,0,0]) translate(-mirror_pivot) children();
    }
}

module mirror_lever_az(){
    // Lever responsible for rotating the mirror about the Z axis
    difference(){
        union(){
            // bottom part: the lever
            translate(zeroz(mirror_pivot)) hull(){
                translate([ 5-mirror_holder_w/2,  -5-zflex[1]/2, 0]) cylinder(r=5, h=base_t);
                translate([-5+mirror_holder_w/2, -5-zflex[1]/2, 0]) cylinder(r=5, h=base_t);
                translate([mirror_lever_az_l, -3-zflex[1]/2, 0]) cylinder(r=3, h=base_t);
                translate([-mirror_holder_w/2,  -10-zflex[1]/2, base_t]) cube([mirror_holder_w, 10, d]); // NB this joins the lever to the top part
            }
            // top part: joins to the mirror mount
            translate(zeroz(mirror_pivot)) hull(){
                translate([-mirror_holder_w/2,  -10-zflex[1]/2, base_t]) cube([mirror_holder_w, 10, d]); // NB this joins the lever to the top part
                translate([-mirror_holder_w/2,  -5-xflex[1]/2, beam_z - 1]) cube([mirror_holder_w, 5, 2]); // NB this joins the lever to the top part
            }
        }

        // cut-out for the beam
        translate(mirror_pivot) rotate([90,0,0]) cylinder(d=mirror_holder_w - 2*xflex[0], h=999, $fn=32);

        // cut-out for the about-x actuating rod
        //translate(zeroz(mirror_pivot)) rotate([90,45,0]) cube([10,10,999], center=true);
        grow() hull() mirror_locus(angle_x=0) mirror_pushstick_ax();

    }
}
mirror_lever_az_min_y = mirror_pivot[1] - zflex[1]/2 -10 - mirror_lever_az_l * sin(mirror_tilt);

mirror_lever_ax_max_y = mirror_lever_az_min_y - mirror_lever_az_l * sin(mirror_tilt) - 3; //go beyond this and risk fouling the other lever 
                                                        //(additional -3 makes the thin wall slightly thicker so it doesn't fail to print)
mirror_lever_ax_overall_l = mirror_lever_az_l + 30; // length of the X lever - the 30 is spacing between gears.
mirror_lever_ax_pivot = [mirror_pivot[0] + mirror_lever_ax_overall_l * 1/(1+mirror_lever_az_l/beam_z), // pivot point of the lever
                                 mirror_lever_ax_max_y-10-zflex[1]/2, base_t/2];

module mirror_lever_ax(){
    // Lever responsible for rotating the mirror around x axis
    max_y = mirror_lever_ax_max_y;
    sequential_hull(){
        translate([mirror_pivot[0], max_y-3, 0]) cylinder(r=3, h=6, $fn=12);
        translate(zeroz(mirror_lever_ax_pivot) + [0,zflex[1]/2+5,0]) cylinder(r=5, h=base_t, $fn=24);
        translate([mirror_pivot[0] + mirror_lever_ax_overall_l, max_y-3, 0]) cylinder(r=3, h=base_t, $fn=12);
    }
}
module mirror_lever_ax_locus(angle_z=mirror_tilt){
    // Tilt the mirror lever back and forth about its pivot
    translate(mirror_lever_ax_pivot) for(az=[-1,0,1]){
        rotate([0,0,az*angle_z]) translate(-mirror_lever_ax_pivot) children();
    }
}

module mirror_pushstick_ax(){
    // Rigid beam connecting the end of the mirror_lever_ax to the bottom of the mirror_holder
    hull(){
        translate([mirror_pivot[0], mirror_lever_ax_max_y+zflex[1] + 2, 0]) cylinder(h=5, r=2, $fn=12);
        translate(mirror_holder_ax_anchor + [0, -2-zflex[1],0]) cylinder(h=3, r=2, $fn=12);
    }
}

mirror_az_anchor_base_y = mirror_holder_back[1] + mirror_bottom_travel; // the rigid base should start here
mirror_az_anchor_z = mirror_az_anchor_base_y - mirror_pivot[1]; // distance from the pivot to the rest of the rigid base
                                      // this is also the height of the bottom, so it rises at about 45 degrees
module mirror_az_anchor(){
    // This rigid part connects to the pivot around Z-axis for the mirror
    base_y = mirror_az_anchor_base_y;
    assert(base_y + 5 < base_max_y, "You probably need to increase base_max_y to include the mirror_az_anchor");
    p = mirror_az_anchor_z;
    hull(){
        translate(zeroz(mirror_pivot) + [0,2,p]) cylinder(r=2, h=base_t - p + 10, $fn=12);
        translate([mirror_pivot[0] - p, base_y, 0]) cube([2*p, 12, base_t+10]);
    }
}

mirror_nut_y = base_max_y-mirror_actuator_travel-8;
module mirror_nut_seat_ax(){
    // Holds the nut that actuates the mirror about the X axis (i.e. vertical tilt)
    translate([mirror_pivot[0]+mirror_lever_az_l, mirror_nut_y,0]) nut_seat_y(mirror_nut_y-mirror_pivot[1]-zflex[1]/2);
}
module mirror_nut_seat_az(){
    // Holds the nut that actuates the mirror about the Z axis (i.e. horizontal motion)
    translate([mirror_pivot[0]+mirror_lever_ax_overall_l, mirror_nut_y,0]) nut_seat_y(mirror_nut_y-mirror_lever_ax_max_y-zflex[1]);
}
module mirror_nut_seats(){
    mirror_nut_seat_ax();
    mirror_nut_seat_az();
}
module mirror_screw_holes(){
    // the actuating screws enter the part here
    translate([mirror_pivot[0]+mirror_lever_az_l, mirror_nut_y,base_t/2]) cylinder_with_45deg_top(999, 3/2*1.1); 
    translate([mirror_pivot[0]+mirror_lever_ax_overall_l, mirror_nut_y,base_t/2]) cylinder_with_45deg_top(999, 3/2*1.1); 
}
module mirror_nut_seat_locus(){
    // move the nut seats back and forth to calculate their locus
    repeat([0,2*mirror_actuator_travel,0],2,center=true) children();
}

module full_height_zflex(l=8){
    // Flexures that bend around the z axis, at the top and bottom of the base
    translate([0,0,base_t/2]) reflect([0,0,1]) translate([0,0,base_t/2-zflex[2]/2-0.5]) cube([zflex[0], l, zflex[2]], center=true);
}
module mirror_flexures(){
    // All the thin bits connecting the various different mirror parts together
    
    // mirror holder to mirror_lever_az
    translate(mirror_pivot) reflect([1,0,0]) translate([mirror_holder_w/2-xflex[0]/2,0]) cube([xflex[0], 5, xflex[2]], center=true);

    // mirror_az_anchor to mirror_lever_az
    translate(zeroz(mirror_pivot) + [0,0,mirror_az_anchor_z+2]) cube([zflex[0], 8, 4], center=true);
    translate(zeroz(mirror_pivot) + [0,0,base_t+10-2]) cube([zflex[0], 8, 4], center=true);

    // mirror holder to mirror_pushstick_ax
    translate(mirror_holder_ax_anchor + [0,0,0.5+0.5]) cube([0.8, 8, 0.6], center=true);

    // mirror_lever_ax to mirror_pushstick_ax
    translate([mirror_pivot[0], mirror_lever_ax_max_y+zflex[1]/2, 1+0.5]) cube([zflex[0], 8, 2], center=true);
    //translate([mirror_pivot[0], mirror_lever_ax_max_y+zflex[1]/2, 8-1]) cube([zflex[0], 8, 2], center=true);

    // mirror_nut_seat_ax to mirror_lever_ax
    translate(zeroz(mirror_pivot) + [mirror_lever_az_l, 0, 0]) full_height_zflex();
    // mirror_nut_seat_az to mirror_lever_az
    translate([mirror_pivot[0] + mirror_lever_ax_overall_l, mirror_lever_ax_max_y, 0]) full_height_zflex();

    // mirror_lever_az to anchor
    translate(zeroz(mirror_lever_ax_pivot)) full_height_zflex();

}

module mirror_support_ties(){
    // thin ties to support the mirror parts during printing
    // stop the mirror holder falling over
    translate(zeroz(mirror_pivot)) reflect([1,0,0]) translate([mirror_holder_w/2, 4, 1.5]) cube([6, 1.6, 0.5], center=true); 
    // anchor the thin wall in between, and the two long levers
    translate([mirror_pivot[0]+12, mirror_lever_az_min_y, 2]) cube([1.6, 16, 0.5], center=true);
    // anchor the thin wall in between, and the two long levers
    // tie between mirror levers
    translate([mirror_pivot[0]+12+50, mirror_lever_az_min_y, 2]) cube([1.6, 16, 0.5], center=true);
    // tie at end of mirror lever
    translate([mirror_pivot[0]+12+80, mirror_lever_az_min_y-12, 2]) cube([1.6, 25, 0.5], center=true);
    // ties at lever actuators
    translate([mirror_pivot[0]+12+90, mirror_lever_az_min_y+45, 2]) cube([25, 1.6, 0.5], center=true);
    translate([mirror_pivot[0]+12+60, mirror_lever_az_min_y+45, 2]) cube([25, 1.6, 0.5], center=true);
}

module mirror_clearance(holes=true){
    // cut-out for the mirror
    grow() union(){
        hull() mirror_locus() mirror_holder();
        hull() mirror_locus(angle_x=0) mirror_lever_az();
        hull() mirror_lever_ax_locus() mirror_lever_ax();
        hull() mirror_nut_seat_locus() mirror_nut_seat_ax();
        hull() mirror_nut_seat_locus() mirror_nut_seat_az();
        mirror_pushstick_ax();
    }
    if(holes) mirror_screw_holes();
}

module mirror_mount_complete(){
    mirror_holder();
    mirror_lever_az();
    mirror_lever_ax();
    mirror_pushstick_ax();
    mirror_az_anchor();
    mirror_nut_seats();
    mirror_flexures();
    mirror_support_ties();
}

///////////////////////////////////////// HERE STARTETH THE LINEAR TRANSLATION PART ///////////////////////////////

linear_carriage_centre = [baseline, 0, 0];
linear_carriage_size = [40, 25, base_t];
linear_carriage_max_y = linear_carriage_centre[1] + linear_carriage_size[1]/2;
linear_carriage_min_x = linear_carriage_centre[0] - linear_carriage_size[0]/2;

module linear_carriage(){
    // The part that moves linearly (with the fixed mirror on it)
    size = linear_carriage_size;
    difference(){
        translate(linear_carriage_centre-zeroz(size)/2) cube(size);

    }
}
module linear_carriage_locus(extra_travel=0){
    // Move the half carriage(s) back and forth
    repeat([linear_travel*2 + extra_travel*2,0,0],2,center=true) children();
}

linear_half_carriage_width = linear_carriage_size[0] + 2*linear_travel + 2*zflex[0] + 4;
linear_half_carriage_min_y = linear_carriage_centre[1] + linear_carriage_size[1]/2 + linear_flexure_z_l;
module linear_half_carriage_plusy(){
    // The rigid block that joins the ends of the flexures, +y from the carriage
    // it moves half as far as the stage (ideally)
    translate([linear_carriage_centre[0]-linear_half_carriage_width/2,
               linear_half_carriage_min_y, 0]) cube([linear_half_carriage_width, 4, base_t]);
}
module linear_half_carriage_locus(){
    // Move the half carriage(s) back and forth
    repeat([linear_travel,0,0],2,center=true) children();
}

module linear_flexure_y(){
    // One nicely filleted linear flexure along y
    rc = 1.5; //radius of curvature
    w = zflex[0];
    h = base_t;
    l = linear_flexure_z_l;
    difference(){
        translate([-rc,-d,0]) cube([w+2*rc, l+2*d, h]);

        repeat([w+2*rc,0,0], 2) hull() repeat([0,l-2*rc,0], 2){
            translate([-rc, rc, -1]) cylinder(r=rc, h=999, $fn=16);
        }
    }
}

module linear_flexures_z(){
    // Leafspring flexures to support the moving carriage
    w = zflex[0];
    h = base_t;
    l = linear_flexure_z_l;
    translate(linear_carriage_centre) reflect([1,0,0]) difference(){
        union(){ // the flexures (NB there are unwanted fillets)
            translate(zeroz(linear_carriage_size)/2 - [w, 0, 0]) linear_flexure_y();
            translate(zeroz(linear_carriage_size)/2 + [linear_travel, 0, 0]) linear_flexure_y();
        }
        translate(zeroz(linear_carriage_size)/2 - [0, 5, 5]) cube([linear_travel, 10, 999]); 
    }
}

linear_stage_anchors_z_w = 10; //width of the linear stage anchors
module linear_stage_anchors_z(){
    // The leafspring flexures (with compliant axis z) anchor to these blocks
    h = linear_carriage_size[1];
    w = linear_stage_anchors_z_w;
    difference(){
        translate(linear_carriage_centre) reflect([1,0,0]) {
            translate([linear_carriage_size[0]/2 + linear_travel, -h/2, 0]) cube([w, h, base_t]);
        }
        // cut a hole for the push-rod
        translate(linear_carriage_centre + [      linear_carriage_size[0]/2 + linear_travel + 3-20,-4.5,base_t/2-4.5]) cube([200, 9, 9]);
    }
}

module linear_stage_clearance(holes=true){
    grow() hull(){
        linear_half_carriage_locus()    linear_half_carriage_plusy();
        linear_carriage_locus(extra_travel=zflex[0]+2) linear_carriage();
    }
}

module linear_stage_plus_mirror_stand(){
    linear_carriage();
    linear_half_carriage_plusy();
    linear_flexures_z();
    linear_stage_anchors_z();
    translate([baseline -14.5-4.5, -12.5, base_t])cube([24, 25, 6]);
}

module mirror_screw_slot(){
    $fn = 25;
    translate([-7.85, 0, 0])hull(){
        cylinder(h=base_t+7, r=2.15);
        translate([15.7, 0, 0])cylinder(h=base_t+7, r=2.15);
    }
}

module nut_hole(){
    translate([-12, -3.6, 0])cube([24,7.2,3.4]);
}

module linear_stage_complete(){
    difference(){
    $fn = 25;
    linear_stage_plus_mirror_stand();
    translate([baseline - 7, 0, 0])mirror_screw_slot();
    translate([baseline - 7, 0, base_t])nut_hole();
}
}

///// Here starts the beamsplitter mount ///////

module beamsplitter_mount(){
    // Mount to accomodate beamsplitter

    difference(){
        rotate([0, 0, -45])translate([0, -17.5, 0]) {
            
            difference(){
            cube([6, 35, 72.5]);
            translate([0,17.5,55])rotate([0,90,0])cylinder(d=27.4, h=6, $fn=32);
            translate([3,17.5,66.5])cylinder(d=3.3, h=6, $fn=12);
        }

        translate([0,10.6,43.1])rotate([0,90,0])cylinder(d=2, h=6, $fn=12);
        translate([0,24.4,43.1])rotate([0,90,0])cylinder(d=2, h=6, $fn=12);
        }
        translate([-17.5, -15.5, 0])rotate([0, 0, -90])cube([6, 35, 72.5]);
    }
}

///// Here starts the compensating plate mount ///////

module compensating_plate_mount(){
    // Mount to accomodate compenasting plate

    translate([0,27,0]){

        
        rotate([0, 0, -45])translate([0, -17.5, 0]) {
            
            difference(){
            cube([6, 35, 72.5]);
            translate([0,17.5,55])rotate([0,90,0])cylinder(d=27.4, h=6, $fn=32);
            translate([3,17.5,66.5])cylinder(d=3.3, h=6, $fn=12);
        }

        translate([0,10.6,43.1])rotate([0,90,0])cylinder(d=2, h=6, $fn=12);
        translate([0,24.4,43.1])rotate([0,90,0])cylinder(d=2, h=6, $fn=12);
        }
        
    }
}
    

///// Here starts the linear stage lever mechanisim /////

module linear_stage_push_rod(){
    // Rigid beam between flexures connecting linear stage to lever
    rotate([0, 0, 180]) difference(){
        hull(){
            $fn = 25;
            cylinder(h=base_t, r=2.5);
            translate([30.5, 0, 0])cylinder(h=base_t, r=2.5);
    }
    translate([4.5,-2.5, 0])cube([22, 5, 10]);
    translate([4.5,-2.5, 15])cube([22, 5, 10]);
    }
}

module linear_stage_lever(){
    // Lever responsible for moving the linear stage. Eenables a gearing reduction of a factor of 4
    translate([0, 100, 0])rotate([0, 0, -90])hull(){
        $fn = 25;
        cylinder(h=base_t, r=2.5);
        translate([80, 0, 0])cylinder(h=base_t, r=5);
        translate([100, 0, 0])cylinder(h=base_t, r=2.5);
    }
}
    
module linear_stage_lever_flexures(){
    translate([baseline+20, 0.75, 2.5])rotate([0, 0, -90])cube([0.8, 2.5, 4]); // bottom flexure connecting push rod to linear stage
    translate([baseline+20, 0.75, 18.5])rotate([0, 0, -90])cube([0.8, 2.5, 4]); // top flexure connecting push rod to linear stage
    translate([baseline+20+43-7, 0.75, 2.5])rotate([0, 0, -90])cube([0.8, 3.5, 4]); // bottom flexure connecting push rod to lever
    translate([baseline+20+43-7, 0.75, 18.5])rotate([0, 0, -90])cube([0.8, 3.5, 4]); // top flexure connecting push rod to lever
    translate([baseline+20+43-2.5-7, 0.75+20, 2.5])rotate([0, 0, -90])cube([0.8, 3.5, 4]); // bottom flexure connecting lever to anchor
    translate([baseline+20+43-2.5-7, 0.75+20, 18.5])rotate([0, 0, -90])cube([0.8, 3.5, 4]); // top flexure connecting lever to anchor
    translate([baseline+20+43-2.5+9-7, 0.75+100, 2.5])rotate([0, 0, -90])cube([0.8, 3.5, 4]); // botttom flexure connecting lever to nut seat
    translate([baseline+20+43-2.5+9-7, 0.75+100, 18.5])rotate([0, 0, -90])cube([0.8, 3.5, 4]); // top flexure connecting lever to nut seat
}

module linear_stage_lever_anchor(){
    // the flexures about which the lever pivots are fixed to this anchor
    translate([baseline+61.5-7, 20-5, 0]){
        rotate([0, 0, 180])translate([0, -10, 0])cube([8, 15, base_t]);
    };
}

module linear_stage_lever_nut_seat(){
    // Holds the nut that actuates the linear stage
    translate([baseline+82-7, 100, 0])rotate([0, 0, -90])translate([0, 30, 0])nut_seat_y(40);
}

linear_stage_lever_pivot = [-5, 20, base_t/2];
lever_tilt = 6;

module linear_stage_lever_locus(angle_z=lever_tilt){
    // Tilt the lever back and forth about its pivot
    translate(linear_stage_lever_pivot) for(az=[-1, 0, 1]){
        rotate([0,0,az*angle_z]) translate(-linear_stage_lever_pivot) children();
    }
}

module linear_stage_lever_clearance(){
    grow() hull(){
        linear_stage_lever_locus()linear_stage_lever();   
    }
}   

module linear_stage_lever_nut_seat_locus(){
    // move the nut seat back and forth to calculate its locus
    repeat([2*12,0,0],2,center=true) children();
}

module linear_stage_lever_nut_seat_clearance(){
    grow() hull(){
        linear_stage_lever_nut_seat_locus()linear_stage_lever_nut_seat(); 
    }
}   

module linear_stage_push_rod_locus(){
    // move the push rod back and forth to calculate its locus
    repeat([2*6,0,0],2,center=true) children();
    repeat([0,0,5],2,center=true) children();
}

module linear_stage_push_rod_clearance(){
    grow() hull(){
        linear_stage_push_rod_locus()
        linear_stage_push_rod();
    }
}  

module linear_stage_lever_screw_holes(){
    // the actuating screws enter the part here
    translate([baseline+20+42.5,100,base_t/2])rotate([0,0,-90])cylinder_with_45deg_top(999, 3/2*1.1); 
}

module linear_stage_lever_ties(){
    translate([180, 100, 2]) cube([1.6, 30, 0.5], center=true);
    translate([mirror_pivot[0]+12+120, mirror_lever_az_min_y+10, 2]) cube([50, 1.6, 0.5], center=true);
    translate([mirror_pivot[0]+12+80, -10, 2]) cube([1.6, 10, 0.5], center=true);
    translate([mirror_pivot[0]+12+55, -10, 2]) cube([1.6, 10, 0.5], center=true);
    translate([mirror_pivot[0]+12+55, -10+50, 2]) cube([1.6, 10, 0.5], center=true);
    translate([mirror_pivot[0]+12+80+15, -10+50, 2]) cube([1.6, 10, 0.5], center=true);
}

module linear_stage_lever_complete(){
    $fn = 25;
    difference(){
        translate([baseline+20+1.5+42.5+1.5+2.5-7, 0, 0])linear_stage_lever();
        // linear_stage lever overring_holes
    translate([baseline+20+1.5+42.5+1.5+2.5-7, 100, base_t-10])cylinder(h=10, r=1.25);
    translate([baseline+20+1.5+42.5+1.5+2.5-7, 100, 0])cylinder(h=10, r=1.25);
    }
    translate([baseline+61.5-7 ,0 , 0])linear_stage_push_rod();
    linear_stage_lever_nut_seat();
    linear_stage_lever_flexures();
    linear_stage_lever_anchor();
    linear_stage_lever_ties();
}

//// Here starts the motor mount /////

module motor_mount(){
    translate([baseline+20+38.55+1.5+40+11.5+10+1.5-20, 100+20, 0])cube([20, 20, base_t]);
    
}

module motor_holes(){
    $fn = 25;
    translate([-15, 0, -6])rotate([0, 90, 0])cylinder(h=15, r=1.25);
    translate([-15, 0, 6])rotate([0, 90, 0])cylinder(h=15, r=1.25);
}

///// Here starts the base to house all the sections /////

module base(){
    // the rigid, static block everything sits in
    difference(){
        linear_extrude(base_t) offset(3) hull() projection(){
            mirror_clearance(holes=false);
            linear_stage_clearance(holes=false);
            translate([-25/2,-25/2,0]) cube([25,25,base_t]);
            translate([baseline+20+1.5+42.5+1.5+2.5-7, 0, 0])linear_stage_lever_clearance();
            linear_stage_lever_nut_seat_clearance();
            translate([baseline+61.5 ,0 , 2.5])linear_stage_push_rod_clearance();
            motor_mount();
        }

        mirror_clearance();
        linear_stage_clearance();
        translate([baseline+20+1.5+42.5+1.5+2.5-7, 0, 0])linear_stage_lever_clearance();
        linear_stage_lever_nut_seat_clearance();
        translate([baseline+61.5 ,0 , 2.5])linear_stage_push_rod_clearance();
        linear_stage_lever_screw_holes();
        screw_holes();
        translate([baseline+20+38.55+1.5+40+11.5+10+1.5+5,100+34, base_t/2])motor_holes();
        overring_holes();

    }

}


module overring_holes(){
    $fn = 25;
    //a_x lever overring_holes
    translate([69, 65, base_t-10])cylinder(h=10, r=1.25);
    translate([69, 65, 0])cylinder(h=10, r=1.25);
    //a_z lever overring_holes
    translate([100, 45, base_t-10])cylinder(h=10, r=1.25);
    translate([100, 45, 0])cylinder(h=10, r=1.25);
    // linear_stage lever overring_holes
    translate([baseline+20+1.5+42.5+1.5+2.5-7-57, 100, base_t-10])cylinder(h=10, r=1.25);
    translate([baseline+20+1.5+42.5+1.5+2.5-7-57, 100, 0])cylinder(h=10, r=1.25);
}

module screw_holes(){
    // holes to allow fixing to optics bench
    translate([140, 130, 0])cylinder(h=base_t, r=3.25);
    translate([165, 80, 0])cylinder(h=base_t, r=3.25);
    translate([40, 105, 0])cylinder(h=base_t, r=3.25);
    translate([-10, 30, 0])cylinder(h=base_t, r=3.25);
    //translate([150, 112.5, 0])cylinder(h=base_t, r=3.25);
    //translate([50, 112.5, 0])cylinder(h=base_t, r=3.25);
    //translate([175, 62.5, 0])cylinder(h=base_t, r=3.25);
}


///// Bringing all sections together ////

module generate_interferometer(){
    base();  
    mirror_mount_complete();
    linear_stage_complete();
    linear_stage_lever_complete();
    beamsplitter_mount();
    compensating_plate_mount();
}


///// Cutouts to print in 3 pieces /////

module mirror_mount_cutout(){
    difference(){
        generate_interferometer();
        translate([400,42,0])rotate([-15,0,180])cube([999,999,999]);
        translate([-100,42,base_t])rotate([165,0,0])cube([999,999,999]);
        translate([-100,0,0])cube([999,39.5,999]);

        translate([162,0,0])rotate([0,0,90]){
        translate([400,42,0])rotate([-15,0,180])cube([999,999,999]);
        translate([-100,42,base_t])rotate([165,0,0])cube([999,999,999]);
        translate([-100,0,0])cube([999,39.5,999]);
        }    
        translate([0,40.5-18,base_t/2])rotate([-90,0,0])cylinder(d=5, h=26, $fn=24);

        translate([125,125,base_t/2])rotate([-90,0,45])translate([0,0,-14])cylinder(d=6.5, h=26, $fn=24);
        translate([125,125,base_t/2])rotate([-90,0,45])translate([0,0,12])cylinder(d=13, h=7, $fn=24);

        translate([120,42,0])rotate([0,0,-90])cube([999,999,999]);

    }
    
}

module beam_splitter_cutout(){
    difference(){
        generate_interferometer();
        translate([-200,42,base_t])rotate([-15,0,0])translate([0,0,-base_t/2])cube([999,999,999]);
        translate([-200,42,base_t])rotate([-15,0,0])translate([0,0,-base_t/2])rotate([-60,0,0])cube([999,999,999]);

        translate([-20,0,0])rotate([0,0,-90]){
        translate([-200,42,base_t])rotate([-75,0,0])cube([999,999,999]);
        translate([-200,42,0])rotate([-15,0,0])cube([999,999,999]);
        }

        translate([0,39.5-18,base_t/2])rotate([-90,0,0])cylinder(d=6.5, h=26, $fn=24);
        translate([0,-50,base_t/2])rotate([-90,0,0])cylinder(d=13, h=50+39.5-18, $fn=24);

        translate([10,0,base_t/2])rotate([0,90,0])cylinder(d=6.5, h=26, $fn=24);
        translate([-20,0,base_t/2])rotate([0,90,0])cylinder(d=13, h=30, $fn=24);

        translate([24.5,-20,0])cube([999,999,999]);

    }
    
}

module linear_stage_cutout(){
    difference(){
        generate_interferometer();
        translate([42+22,0,0])rotate([0,0,90])translate([-200,42,base_t])rotate([-15,0,0])translate([0,0,-base_t/2])cube([999,999,999]);
        translate([42+22,0,0])rotate([0,0,90])translate([-200,42,base_t])rotate([-15,0,0])translate([0,0,-base_t/2])rotate([-60,0,0])cube([999,999,999]);

        translate([0,0,base_t+8])cube([999,999,999]);

        translate([120,42,base_t])rotate([-15,0,0])translate([0,0,-base_t/2])rotate([0,0,90])cube([999,999,999]);
        translate([120,42,base_t])rotate([-15,0,0])translate([0,0,-base_t/2])rotate([0,0,90])rotate([0,60,0])cube([999,999,999]);

        translate([120,42,base_t])rotate([0,-15,0])translate([0,0,-base_t/2])rotate([0,0,90])cube([999,999,999]);
        translate([120,42,base_t])rotate([0,-15,0])translate([0,0,-base_t/2])rotate([0,0,90])rotate([-60,0,0])cube([999,999,999]);

        translate([10,0,base_t/2])rotate([0,90,0])cylinder(d=5, h=28, $fn=24);

        translate([125,125,base_t/2])rotate([-90,0,45])translate([0,0,-14])cylinder(d=5, h=26, $fn=24);
    }
    
}

//linear_stage_cutout();
//beam_splitter_cutout();
//mirror_mount_cutout();
generate_interferometer();